

// ARRAYS
/*
- An array in programming is simply a list of data.
- They are declared using square brackets [], also known as "Array literals"
- They are usually used to store numerous amounts of data to manipulate in order to perform a number of tasks.
- Arrays provide a number of functions(or methods in other words) that help in achieving this.
- The main difference of an array to an obkect is that it contains information in a form of "list", unlike objects that uses properties
- Syntax:
	let/const arrayName = [elementA, elementB, elementC, etc..]
*/

let studentNumberA = '2023-1923';
let studentNumberB = '2023-1924';
let studentNumberC = '2023-1925';
let studentNumberD = '2023-1926';
let studentNumberE = '2023-1927';

//With Array

let studentNumbers = ['2023-1928', '2023-1929', '2023-1930'];
console.log(studentNumbers);
// Common examples of an array
let grade = [98.5, 94.3, 99.01, 90.1]
console.log(grade);
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"]
console.log(computerBrands);
// Array with mixed data types (not recommended)
let mixedArr = [12, "Asus", null, undefined, {}]
console.log(mixedArr);

//Alternative way to write arrays
let myTasks = [
		'drink HTML',
		"eat javacript",
		'inhale css',
		"bake sass"
	];
console.log(myTasks);

// Creating an array with values from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";
let cities = [city1, city2, city3];
console.log(cities);

// Length Property
// The .length property allows us to "get" and set the total number of items in an array.

console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);
// .length property can also be used with strings. Some array methods and properties can be used with strings.
let fullName = "Zybren Carbonilla";
console.log(fullName.length);

// .length property can also set the total number of items in an array. Meaning, we can actually delete the last item in the array or shorten the array by updating its length.
myTasks.length = myTasks.length - 1;
console.log(myTasks.length);
console.log(myTasks);

// Using decrementation
cities.length--;
console.log(cities);
cities.length--;
console.log(cities);


// Not applicable on strings
fullName.length = fullName.length - 1;
console.log(fullName);
fullName.length--;
console.log(fullName);

// Adding an item in an array
// if you can shortened the array by setting the length property, we can also lengthen it by adding a number into the length property. But it will be undefined.

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length++;
console.log(theBeatles);

// Reading from Arrays
/*
- We can access array elements throught the use of array indexes
- In Javascript, the first element is associated with the number 0.
- Array indexes actually referred to an address/location in the device's memory and how the information is stored
- Syntax:
	arrayName[index]
*/

console.log(grade[0]);
console.log(computerBrands[1]);

// Accessing an array element that does not exist will return "undefined"
console.log(grade[20]);

let lakersLegend = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"]

console.log(lakersLegend[1]);
console.log(lakersLegend[3]);
console.log(lakersLegend[lakersLegend.length - 1]);

// You can save or store array items in another variable
let currentLaker = lakersLegend[2];
console.log(currentLaker);

// You can re-assign array values using item's indices
console.log("Array before assignment: ");
console.log(lakersLegend);
lakersLegend[2] = "Paul Gasol";
console.log("After assignment");
console.log(lakersLegend);

// Accessing the last element

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
let lastElementIndex = bullsLegends.length-1
console.log(bullsLegends[lastElementIndex]);
console.log(bullsLegends[bullsLegends.length-1]);

// Adding Items Into the Array
// Using indices, we can add items into the array
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

newArr[1] = "Jeru Palma";
console.log(newArr);

newArr[2] = "Mica Tingcang"
console.log(newArr);

//We can add items at the end of the array instead of adding it in the front to avoid the risk of replacing the first items in the array.

newArr[newArr.length] = "Jeno Vea";
console.log(newArr);

for (let index = 0; index < newArr.length; index++){
	console.log(newArr[index]);
}

let numArr = [5, 12, 30 , 46, 40];
for (let index = 0; index < numArr.length; index++){
	if (numArr[index] % 5 === 0){
		console.log(numArr[index] + " is divisible by 5")
	} else {
		console.log(numArr[index] + " is not divisible by 5");
	}
}

// Multidimensional Arrays
/*
- Multidimensional arrays are useful for storing complex data structure
*/

let chessBoard = [
		['a1', 'b1', 'c1', 'd1', 'e1' , 'f1', 'g1', 'h1'],
		['a2', 'b2', 'c2', 'd2', 'e2' , 'f2', 'g2', 'h2'],
		['a3', 'b3', 'c3', 'd3', 'e3' , 'f3', 'g3', 'h3'],
		['a4', 'b4', 'c4', 'd4', 'e4' , 'f4', 'g4', 'h4'],
		['a5', 'b5', 'c5', 'd5', 'e5' , 'f5', 'g5', 'h5'],
		['a6', 'b6', 'c6', 'd6', 'e6' , 'f6', 'g6', 'h6'],
		['a7', 'b7', 'c7', 'd7', 'e7' , 'f7', 'g7', 'h7'],
		['a8', 'b8', 'c8', 'd8', 'e8' , 'f8', 'g8', 'h8'],
		['a9', 'b9', 'c9', 'd9', 'e9' , 'f9', 'g9', 'h9']
	]

console.log(chessBoard[2][2]);

console.log("Pawn moves to " + chessBoard[1][5]);